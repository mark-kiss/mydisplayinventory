const { init, shutdown } = require('./web/server');
const logger = require('./logger');
const { connect, disconnect } = require('./db');

async function startup() {
  try {
    await init();
    await connect();

    process.once('SIGINT', () => {
      shutdown(async () => {
        try {
          await disconnect();
          logger.info('Database was succesfully disconnected');
          process.exit(0);
        } catch (ex) {
          logger.error('Could not disconnect from the database');
          process.exit(1);
        }
      });
    });
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
}

startup();
