const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const { promisify } = require('util');
const { publicRouter } = require('./router');
const { port } = require('../config');
const logger = require('../logger');

const app = express();
const server = http.createServer(app);


app.use(bodyParser.json());
app.use((req, res, next) => {
  logger.info(`${req.method} ${req.url} at ${new Date()}`);
  next();
});

app.use(publicRouter);

const listenPromise = promisify(server.listen.bind(server, port));

function shutdown(cleanup) {
  server.close(() => {
    cleanup();
  });
}

module.exports = {
  init: listenPromise,
  shutdown,
};


