const { Router } = require('express');

const publicRouter = Router();

publicRouter.get('/', (req, res) => {
  res.send({
    message: 'OK',
  });
});

module.exports = { publicRouter };
