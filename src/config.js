const joi = require('joi');

const schema = joi
  .object({
    PORT: joi.number().default(3000),
    LOG_LEVEL: joi.string().default('info'),
    DB_URI: joi.string().required(),
  })
  .unknown()
  .required();

const validatedConfig = joi.attempt(process.env, schema);

const config = {
  port: validatedConfig.PORT,
  logLevel: validatedConfig.LOG_LEVEL,
  dbURI: validatedConfig.DB_URI,
};

module.exports = config;
